<?php
require "vendor/autoload.php";
use Michelf\Markdown;

Flight::before('start', function($params, $output){
    ORM::configure('sqlite:data.sqlite3');
});

Flight::route('/', function(){
    $data = [
        'frame' => Warframes::find_many(), 
    ];

    Flight::view()->display('frames.twig', $data);
});

Flight::route('/add', function(){
    Flight::view()->display('Add.twig');
});

Flight::route('/pouet', function(){
    
    
    Flight::redirect('/');
});

Flight::route('/commentaires', function(){
    $data = [
        'comz' => Commentaires::find_many(), 
    ];
    Flight::view()->display('commentaires.twig', $data);
});

Flight::route('/commentaire/post', function(){

    if (Flight::request()->method == 'POST'){
        

        $get_comz = Model::factory('Commentaires')->create();
        $get_comz->pseudo = Flight::request()->data->pseudo;
        $get_comz->commentaires = Flight::request()->data->commentaires;

        $get_comz->save();
    }
    Flight::redirect('/commentaires');
});


Flight::route('/wf/ayaya', function(){

    if (Flight::request()->method == 'POST'){
        

        $frame_content = Model::factory('Warframes')->create();
        $frame_content->wf_name = Flight::request()->data->wf_name;
        $frame_content->wf_description = Flight::request()->data->wf_description;
        $frame_content->wf_img = Flight::request()->data->wf_img;
        $frame_content->wf_pv = Flight::request()->data->wf_pv;
        $frame_content->wf_armor = Flight::request()->data->wf_armor;
        $frame_content->wf_ms = Flight::request()->data->wf_ms;
        $frame_content->wf_shield = Flight::request()->data->wf_shield;
        $frame_content->wf_energy = Flight::request()->data->wf_energy;

        $frame_content->save();
    }
    Flight::redirect('/commentaires');
});

Flight::route('/com/@com_id', function($com_id){

    if (Flight::request()->method == 'POST'){

        $comz = Model::factory('Commentaires')->where('com_id', $com_id)->find_one();

        $comz->delete();
}
Flight::redirect('/commentaires');

});

Flight::route('/@WF_id', function($WF_id){
    $data = [
        'Sgframe' =>  Model::factory('Warframes')->where('wf_id', $WF_id)->find_one(),
    ];
    Flight::view()->display('Sframe.twig', $data);
});

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
});

Flight::start();
?>